package edu.ifrs.canoas.layouts;

import android.app.Activity;
import android.os.Bundle;

/**
 * Classe extende uma Activity 
 */
public class MainActivity extends Activity {

	/**
	 * Método que será chamado ao criar a Activity
	 * 
	 * @param Bundle O ultimo estado dessa atividade
	 */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState); //
		setContentView(R.layout.activity_main); //aponta o nome do xml com layout
	
	}
}